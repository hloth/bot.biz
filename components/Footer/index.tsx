import Link from 'next/link'
import styles from './styles.module.scss'
import { useTranslation } from 'next-i18next'

export default function Footer() {
  const { t } = useTranslation('common')
  
  return (
    <footer className={styles.footer}>
      <Link
        href=''
      >
        <a>
          {t('footer.userAgreement')}
        </a>
      </Link>
      <span>{t('footer.copyright')}</span>
    </footer>
  )
}