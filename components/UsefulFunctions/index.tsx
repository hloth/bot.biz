import React from 'react'
import PropTypes from 'prop-types'
import styles from './styles.module.scss'
import { useTranslation } from 'next-i18next'
import Subheading from '%/components/Subheading'
import Button from '%/components/Button'
import { Masonry } from '@mui/lab'
import { useMediaQuery } from '@mui/material'
import Slider from '%/components/Slider'
import SlickControls from '%/components/SlickControls'
import Fade from 'react-reveal'

import { ReactComponent as ClockIcon } from './Icons/clock.svg'
import { ReactComponent as MessageIcon } from './Icons/message.svg'
import { ReactComponent as MovieIcon } from './Icons/movie.svg'
import { ReactComponent as MailIcon } from './Icons/mail.svg'
import SectionLine from '%/components/HashScroller/SectionLine'

interface FeatureInfo {
  icon: React.ReactNode
  text: string
}

interface FunctionInfo {
  icon: React.ReactNode
  title: string
  features: FeatureInfo[]
}

const allFunctions: { [key: string]: FunctionInfo[] } = {
  bots: [
    {
      icon: <MailIcon />,
      title: 'Рассылка внутри ботов',
      features: [
        {
          icon: <ClockIcon />,
          text: 'Отложенная рассылка (рассылка с таймером на определенное время)'
        },
        {
          icon: <MovieIcon />,
          text: 'Переменные для рассылки'
        },
      ]
    },
    {
      icon: <MailIcon />,
      title: 'Рассылка внутри ботов',
      features: [
        {
          icon: <ClockIcon />,
          text: 'Отложенная рассылка (рассылка с таймером на определенное время)'
        },
        {
          icon: <MovieIcon />,
          text: 'Переменные для рассылки'
        },
        {
          icon: <MovieIcon />,
          text: 'Переменные для рассылки'
        },
        {
          icon: <MovieIcon />,
          text: 'Переменные для рассылки'
        },
      ]
    },
    {
      icon: <MailIcon />,
      title: 'Функция без названия',
      features: [
        {
          icon: <MovieIcon />,
          text: 'Переменные для рассылки'
        },
        {
          icon: <MessageIcon />,
          text: 'Переменные для рассылки'
        },
      ]
    },
    {
      icon: <MailIcon />,
      title: 'Функция без названия',
      features: [
        {
          icon: <MovieIcon />,
          text: 'Переменные для рассылки'
        },
        {
          icon: <MessageIcon />,
          text: 'Переменные для рассылки'
        },
        {
          icon: <MessageIcon />,
          text: 'Переменные для рассылки'
        },
      ]
    },
    {
      icon: <MailIcon />,
      title: 'Функция без названия',
      features: [
        {
          icon: <MovieIcon />,
          text: 'Переменные для рассылки'
        },
        {
          icon: <MessageIcon />,
          text: 'Переменные для рассылки'
        },
      ]
    }
  ],
  sites: [
    {
      icon: <MailIcon />,
      title: 'Функция без названия',
      features: [
        {
          icon: <MovieIcon />,
          text: 'Переменные для рассылки'
        },
        {
          icon: <MessageIcon />,
          text: 'Переменные для рассылки'
        },
      ]
    },
    {
      icon: <MailIcon />,
      title: 'Функция без названия',
      features: [
        {
          icon: <MovieIcon />,
          text: 'Переменные для рассылки'
        },
        {
          icon: <MessageIcon />,
          text: 'Переменные для рассылки'
        },
      ]
    },
    {
      icon: <MailIcon />,
      title: 'Функция без названия',
      features: [
        {
          icon: <MovieIcon />,
          text: 'Переменные для рассылки'
        },
        {
          icon: <MessageIcon />,
          text: 'Переменные для рассылки'
        },
      ]
    },
    {
      icon: <MailIcon />,
      title: 'Функция без названия',
      features: [
        {
          icon: <MovieIcon />,
          text: 'Переменные для рассылки'
        },
        {
          icon: <MessageIcon />,
          text: 'Переменные для рассылки'
        },
      ]
    }
  ],
  apps: [
    {
      icon: <MailIcon />,
      title: 'Функция без названия',
      features: [
        {
          icon: <MovieIcon />,
          text: 'Переменные для рассылки'
        },
        {
          icon: <MessageIcon />,
          text: 'Переменные для рассылки'
        },
      ]
    },
    {
      icon: <MailIcon />,
      title: 'Функция без названия',
      features: [
        {
          icon: <MovieIcon />,
          text: 'Переменные для рассылки'
        },
        {
          icon: <MessageIcon />,
          text: 'Переменные для рассылки'
        },
      ]
    },
    {
      icon: <MailIcon />,
      title: 'Функция без названия',
      features: [
        {
          icon: <MovieIcon />,
          text: 'Переменные для рассылки'
        },
        {
          icon: <MessageIcon />,
          text: 'Переменные для рассылки'
        },
      ]
    }
  ]
}

export default function UsefulFunctions() {
  const { t } = useTranslation('homePage')
  const [selectedTab, setSelectedTab] = React.useState('bots')
  const [functions, setFunctions] = React.useState(allFunctions['bots'])
  const [currentPage, setCurrentPage] = React.useState(0)
  const slickRef = React.useRef()
  const m1200 = useMediaQuery('(max-width:1200px)')
  const m650 = useMediaQuery('(max-width:650px)')

  React.useEffect(() => {
    const selectedFunctions = allFunctions[selectedTab]
    setFunctions(selectedFunctions)
  }, [selectedTab])
  
  return (
    <div className={styles.usefulFunctions}>
      <Subheading>{t('blocks.usefulFunctions.heading')}</Subheading>
      <SectionLine id='useful-functions' />
      <Fade bottom cascade>
        <div className={styles.tabs}>
          <Button
            outlined={selectedTab !== 'bots'}
            buttonProps={{ onClick: () => setSelectedTab('bots') }}
          >
            {t('blocks.usefulFunctions.tabs.bots')}
          </Button>
          <Button
            outlined={selectedTab !== 'sites'}
            buttonProps={{ onClick: () => setSelectedTab('sites') }}
          >
            {t('blocks.usefulFunctions.tabs.sites')}
          </Button>
          <Button
            outlined={selectedTab !== 'apps'}
            buttonProps={{ onClick: () => setSelectedTab('apps') }}
          >
            {t('blocks.usefulFunctions.tabs.apps')}
          </Button>
        </div>  
      </Fade>
      {m650 ? (
        <div className={styles.carousel}>
          <div className={styles.controls}>
            <SlickControls
              currentPage={currentPage}
              maxPages={functions.length}
              slideBack={() => slickRef.current?.prev?.()}
              slideNext={() => slickRef.current?.next?.()}
            />
          </div>
          <div className={styles.slider}>
            <Slider 
              infinite={false}
              variableWidth={false}
              slidesToShow={1} 
              afterChange={setCurrentPage} 
              ref={slickRef} 
              arrows={false}
              // className={styles.slider}
            >
              {functions.map((functionInfo: FunctionInfo, i: number) => (
                <div key={i}><Function functionInfo={functionInfo} /></div>
              ))}
            </Slider>
          </div>
          <div className={styles.sliderPlaceholder} />
        </div>
      ) : (
        <div className={styles.masonryContainer}>
          <Masonry columns={m1200 ? 2 : 3} spacing={4} className={styles.masonry}>
            <Functions functions={functions} />
          </Masonry>
        </div>
      )}
    </div>
  )
}

Functions.propTypes = {
  functions: PropTypes.array
}
interface FunctionsProps {
  functions: FunctionInfo[]
}
function Functions(props: FunctionsProps) {
  return (
    <>
      {props.functions.map((functionInfo: FunctionInfo, i: number) => (
        <Function functionInfo={functionInfo} key={i} />
      ))}
    </>
  )
}

Function.propTypes = {
  functionInfo: PropTypes.object
}
interface FunctionProps {
  functionInfo: FunctionInfo
}
function Function(props: FunctionProps) {
  return (
    <div className={styles.card}>
      <div className={styles.top}>
        <h5>{props.functionInfo.title}</h5>
        <div className={styles.icon}>
          {props.functionInfo.icon}
        </div>
      </div>
      <ul>
        {props.functionInfo.features.map((feature: FeatureInfo, i: number) => (
          <ol key={i}>
            <span>{feature.icon}</span> <span>{feature.text}</span>
          </ol>
        ))}
      </ul>
    </div>
  )
}