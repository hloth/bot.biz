import styles from './styles.module.scss'
import Subheading from '../Subheading'
import { useTranslation } from 'next-i18next'

import SourceConnection from './SourceConnection'
import AttractingUsers from './AttractingUsers'
import SectionLine from '%/components/HashScroller/SectionLine'
import RevenueAndWithdrawal from './RevenueAndWithdrawal'

export default function HowItWorks() {
  const { t } = useTranslation('homePage')
  
  return (
    <div className={styles.howItWorks}>
      <SectionLine id='how-it-works_connection' />
      <Subheading>{t('blocks.howitworks.heading')}</Subheading>
      <ol className={styles.list}>
        <li><SourceConnection /></li>
        <li><AttractingUsers /></li>
        <li><RevenueAndWithdrawal /></li>
      </ol>
    </div>
  )
}