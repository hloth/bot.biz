import styles from './styles.module.scss'
import Step from './Step'
import Image from 'next/image'
import { useTranslation } from 'next-i18next'
import Fade from 'react-reveal/Fade'

import Tablet from './Assets/SourceConnection/tablet.png'
import { ReactComponent as Phone } from './Assets/SourceConnection/phone.svg'
import { ReactComponent as Robot } from './Assets/SourceConnection/robot.svg'
import Logo from '%/components/Logo'

export default function SourceConnection() {
  return (
    <Step i={1}>
      <div className={styles.sourceConnection}>
        <Fade left>
          <div className={styles.tablet} style={{ width: 524, height: 402 }}>
            <Image 
              src={Tablet}
              alt=''
              priority={true}
              width={1048}
              height={804}
              quality={100}
            />
          </div>
        </Fade>
        <div className={styles.phone}>
          <Fade right delay={1000} distance='50px'>
            <Phone />
          </Fade>
        </div>
        <Fade bottom delay={800} distance='50px'>
          <div className={styles.robot}>
            <Robot />
          </div>
        </Fade>
        <Label code='sites' className={styles.sites} delay={200} />
        <Label code='bots' className={styles.bots} delay={400} />
        <Label code='apps' className={styles.apps} delay={600} />
        <Fade bottom delay={500}>
          <div className={styles.logo}>
            <Logo />
          </div>
        </Fade>
      </div>
    </Step>
  )
}

function Label(props: { code: string, className: string, delay?: number }) {
  const { t } = useTranslation('homePage')

  return (
    <Fade bottom delay={props.delay}>
      <div className={[styles.label, props.className].join(' ')}>
        <span>{t('blocks.howitworks.steps.0.' + props.code)}</span>
      </div>
    </Fade>
  )
}