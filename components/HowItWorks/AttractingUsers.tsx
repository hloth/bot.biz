import styles from './styles.module.scss'
import Step from './Step'
import Fade from 'react-reveal/Fade'

import { ReactComponent as Megaphone } from './Assets/AttractingUsers/megaphone.svg'

export default function AttractingUsers() {
  return (
    <Step i={2}>
      <Fade right>
        <div className={styles.attractingUsers}>
          <Megaphone />
        </div>
      </Fade>
    </Step>
  )
}