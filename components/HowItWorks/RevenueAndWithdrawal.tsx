import styles from './styles.module.scss'
import Step from './Step'
import Image from 'next/image'
import Fade from 'react-reveal/Fade'

import Tablet from './Assets/RevenueAndWithdrawal/tablet.png'

export default function RevenueAndWithdrawal() {
  return (
    <Step i={3}>
      <Fade right>
        <div className={styles.revenueAndWithdrawal}>
          <Image
            src={Tablet}
            alt=''
            priority
          />
        </div>
      </Fade>
    </Step>
  )
}