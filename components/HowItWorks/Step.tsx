import PropTypes from 'prop-types'
import styles from './styles.module.scss'
import { useTranslation } from 'next-i18next'
import Fade from 'react-reveal/Fade'

Step.propTypes = {
  i: PropTypes.number,
  children: PropTypes.node
}

interface StepProps {
  i: 1 | 2 | 3
  children: React.ReactNode
}

export default function Step(props: StepProps) {
  const { t } = useTranslation('homePage')
  
  return (
    <div className={styles.step}>
      <div className={styles.info}>
        <Fade left distance='100px'>
          <div><span className={styles.stepIndex}>{props.i}</span></div>
        </Fade>
        <Fade bottom>
          <div>
            <h3>{t(`blocks.howitworks.steps.${props.i - 1}.title`)}</h3>
            <p>{t(`blocks.howitworks.steps.${props.i - 1}.description`)}</p>
          </div>
        </Fade>
      </div>
      {props.children}
    </div>
  )
}