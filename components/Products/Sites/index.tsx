import React from 'react'
import PropTypes from 'prop-types'
import styles from './styles.module.scss'
import Fade from 'react-reveal/Fade'
import { useTranslation } from 'next-i18next'
import Button from '%/components/Button'
import Slider from '%/components/Slider'
import SlickControls from '%/components/SlickControls'
import Image from 'next/image'
import { useMediaQuery } from '@mui/material'
import SectionLine from '%/components/HashScroller/SectionLine'

interface SiteInfo {
  name: string
  image: string
  description: string
}

const sites: SiteInfo[] = [
  {
    name: 'Хостинг провайдер Timeweb',
    image: 'https://picsum.photos/628/348' + '?time1=' + Date.now(),
    description: 'Некоторые особенности внутренней политики освещают чрезвычайно интересные особенности картины в целом, однако конкретные выводы, разумеется, в равной степени предоставлены сами себе. Разнообразный и богатый...'
  },
  {
    name: 'Хостинг провайдер Джино',
    image: 'https://picsum.photos/628/348' + '?time2=' + Date.now(),
    description: 'Ясность нашей позиции очевидна: постоянный количественный рост и сфера нашей активности напрямую зависит от вывода текущих активов! Ясность нашей позиции очевидна: глубокий уровень погружения в значительной...'
  },
  {
    name: 'Хостинг провайдер Спринт Хост',
    image: 'https://picsum.photos/628/348' + '?time3=' + Date.now(),
    description: 'Некоторые особенности внутренней политики освещают чрезвычайно интересные особенности картины в целом, однако конкретные выводы, разумеется, в равной степени предоставлены сами себе. Разнообразный и богатый...'
  },
  {
    name: 'Хостинг провайдер Джино',
    image: 'https://picsum.photos/628/348' + '?time4=' + Date.now(),
    description: 'Ясность нашей позиции очевидна: постоянный количественный рост и сфера нашей активности напрямую зависит от вывода текущих активов! Ясность нашей позиции очевидна: глубокий уровень погружения в значительной... '
  },
  {
    name: 'Хостинг провайдер Спринт Хост',
    image: 'https://picsum.photos/628/348' + '?time5=' + Date.now(),
    description: 'Противоположная точка зрения подразумевает, что некоторые особенности внутренней политики формируют глобальную экономическую сеть и при этом - превращены в посмешище, хотя само их существование... '
  },
]

export default function Sites() {
  const { t } = useTranslation('homePage')
  const [currentPage, setCurrentPage] = React.useState(0)
  const slickRef = React.useRef()
  const matches = useMediaQuery('(max-width:650px)')

  const sitesCount = 9 - sites.length

  return (
    <>
      <SectionLine id='products-sites-section' />
      <Fade bottom>
        <div className={styles.subheading}>
          <h3>{t('blocks.products.sites.heading')}</h3>
          <SlickControls 
            currentPage={currentPage} 
            maxPages={sites.length} 
            slideBack={() => slickRef.current?.prev?.()} 
            slideNext={() => slickRef.current?.next?.()} 
          />
        </div>
      </Fade>
      <div className={styles.cardsContainer}>
        <Fade bottom>
          <div className={styles.carouselContainer}>
            <Slider
              variableWidth={!matches}
              infinite={false}
              afterChange={setCurrentPage}
              ref={slickRef}
              arrows={false}
            >
              {sites.map((site, i) => <div key={i} ><Card site={site} /></div>)}
            </Slider>
          </div>
        </Fade>
      </div>
    </>
  )
}

Card.propTypes = {
  site: PropTypes.object
}
interface CardProps {
  site: SiteInfo
  style?: object
}
function Card(props: CardProps) {
  const { t } = useTranslation('homePage')

  return (
    <div className={styles.card} style={props.style}>
      <div className={styles.image}>
        <Image
          src={props.site.image}
          alt={props.site.name}
          width={626}
          height={348}
          priority
        />
      </div>
      <h4>{props.site.name}</h4>
      <p>{props.site.description}</p>
      <Button>{t('blocks.products.sites.goto')}</Button>
    </div>
  )
}
