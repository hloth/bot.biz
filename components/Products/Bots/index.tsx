import React from 'react'
import PropTypes from 'prop-types'
import styles from './styles.module.scss'
import Fade from 'react-reveal/Fade'
import { useTranslation } from 'next-i18next'
import { ReactComponent as HackerIcon } from './icons/hacker.svg'
import Button from '%/components/Button'
import Slider from '%/components/Slider'
import SlickControls from '%/components/SlickControls'
import SectionLine from '%/components/HashScroller/SectionLine'

interface BotInfo {
  name: string
  icon: React.ReactNode
  description: string
  revenue: string
}

export default function Bots() {
  const { t } = useTranslation('homePage')
  const [currentPage, setCurrentPage] = React.useState(0)
  const slickRef = React.useRef()
  
  const bots: BotInfo[] = [
    {
      name: 'Поиск шкур',
      icon: <HackerIcon />,
      description: 'Внезапно, представители современных социальных резервов набирают популярность среди определенных слоев населения, а значит, должны быть объявлены...',
      revenue: '+120 000 ₽'
    },
    {
      name: 'Поиск шкур',
      icon: <HackerIcon />,
      description: 'Внезапно, представители современных социальных резервов набирают популярность среди определенных слоев населения, а значит, должны быть объявлены...',
      revenue: '+120 000 ₽'
    },
    {
      name: 'Поиск шкур',
      icon: <HackerIcon />,
      description: 'Внезапно, представители современных социальных резервов набирают популярность среди определенных слоев населения, а значит, должны быть объявлены...',
      revenue: '+120 000 ₽'
    },
    {
      name: 'Поиск шкур',
      icon: <HackerIcon />,
      description: 'Внезапно, представители современных социальных резервов набирают популярность среди определенных слоев населения, а значит, должны быть объявлены...',
      revenue: '+120 000 ₽'
    },
    {
      name: 'Поиск шкур',
      icon: <HackerIcon />,
      description: 'Внезапно, представители современных социальных резервов набирают популярность среди определенных слоев населения, а значит, должны быть объявлены...',
      revenue: '+120 000 ₽'
    },
    {
      name: 'Поиск шкур',
      icon: <HackerIcon />,
      description: 'Внезапно, представители современных социальных резервов набирают популярность среди определенных слоев населения, а значит, должны быть объявлены...',
      revenue: '+120 000 ₽'
    },
  ]

  const botsCount = 9 - bots.length

  return (
    <>
      <SectionLine id='products-bots-section' />
      <Fade bottom>
        <div className={styles.subheading}>
          <h3>{t('blocks.products.bots.heading')}</h3>
          <div className={styles.сontrols}>
            <SlickControls 
              currentPage={currentPage} 
              maxPages={bots.length} 
              slideBack={() => slickRef.current?.prev?.()} 
              slideNext={() => slickRef.current?.next?.()} 
            />
          </div>
        </div>
      </Fade>
      <div className={styles.cardsContainer}>
        <Fade bottom>
          <div className={styles.carouselContainer}>
            <Slider afterChange={setCurrentPage} ref={slickRef}>
              {bots.map((bot, i) => <div key={i} ><Card bot={bot} /></div>)}
            </Slider>
          </div>
        </Fade>
      </div>
    </>
  )
}

Card.propTypes = {
  bot: PropTypes.object
}
interface CardProps {
  bot: BotInfo
  style?: object
}
function Card(props: CardProps) {
  const { t } = useTranslation('homePage')

  return (
    <div className={styles.card} style={props.style}>
      <div className={styles.top}>
        <h4>{props.bot.name}</h4> 
        <span className={styles.icon}>{props.bot.icon}</span>
      </div>
      <p>
        {props.bot.description}
      </p>
      <div className={styles.revenue}>
        {props.bot.revenue}
        <br></br>
        {t('blocks.products.bots.revenue_for_month')}
      </div>
      <div className={styles.bottom}>
        <Button>{t('blocks.products.bots.connect_bot')}</Button>
        <Button outlined>{t('blocks.products.bots.demo')}</Button>
      </div>
    </div>
  )
}