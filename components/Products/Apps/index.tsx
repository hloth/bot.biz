import React from 'react'
import PropTypes from 'prop-types'
import styles from './styles.module.scss'
import Fade from 'react-reveal/Fade'
import { useTranslation } from 'next-i18next'
import Button from '%/components/Button'
import Slider from '%/components/Slider'
import SlickControls from '%/components/SlickControls'
import Image, { StaticImageData } from 'next/image'
import { useMediaQuery } from '@mui/material'
import { ReactComponent as AppStoreIcon } from './AppStoreIcon.svg'
import { ReactComponent as GooglePlayIcon } from './GooglePlayIcon.svg'
import cx from 'classnames'

import WhatsappImage from './Logos/whatsapp.png'
import Link from 'next/link'

interface AppInfo {
  name: string
  image: string | StaticImageData
  description: string
  revenue: string
  stores: {
    appstore: string
    googleplay: string
  }
}

const sites: AppInfo[] = [
  {
    name: 'Whatsapp',
    image: WhatsappImage,
    description: 'Некоторые особенности внутренней политики освещают чрезвычайно интересные особенности картины в целом, однако конкретные выводы, разумеется, в равной степени предоставлены сами себе.',
    revenue: '+11 500 ₽',
    stores: {
      appstore: 'https://apple.com',
      googleplay: 'https://google.com',
    }
  },
  {
    name: 'Whatsapp',
    image: WhatsappImage,
    description: 'Противоположная точка зрения подразумевает, что некоторые особенности внутренней политики формируют глобальную экономическую сеть и при этом - функционально',
    revenue: '+20 800 ₽',
    stores: {
      appstore: 'https://apple.com',
      googleplay: 'https://google.com',
    }
  },
  {
    name: 'Whatsapp',
    image: WhatsappImage,
    description: 'Таким образом, существующая теория предопределяет высокую востребованность модели развития. Задача организации, в особенности же глубокий уровень погружения влечет',
    revenue: '+5 300 ₽',
    stores: {
      appstore: 'https://apple.com',
      googleplay: 'https://google.com',
    }
  },
  {
    name: 'Whatsapp',
    image: WhatsappImage,
    description: 'Некоторые особенности внутренней политики освещают чрезвычайно интересные особенности картины в целом, однако конкретные выводы, разумеется, в равной степени предоставлены сами себе.',
    revenue: '+11 500 ₽',
    stores: {
      appstore: 'https://apple.com',
      googleplay: 'https://google.com',
    }
  }
]

export default function Apps() {
  const { t } = useTranslation('homePage')
  const [currentPage, setCurrentPage] = React.useState(0)
  const slickRef = React.useRef()
  const matches = useMediaQuery('(max-width:650px)')

  const sitesCount = 9 - sites.length

  return (
    <>
      <Fade bottom>
        <div className={styles.subheading}>
          <h3>{t('blocks.products.apps.heading')}</h3>
          <SlickControls 
            currentPage={currentPage} 
            maxPages={sites.length} 
            slideBack={() => slickRef.current?.prev?.()} 
            slideNext={() => slickRef.current?.next?.()} 
          />
        </div>
      </Fade>
      <div className={styles.cardsContainer}>
        <Fade bottom>
          <div className={styles.carouselContainer}>
            <Slider 
              afterChange={setCurrentPage}
              ref={slickRef}
              arrows={false}
            >
              {sites.map((app, i) => <div key={i} ><Card app={app} /></div>)}
            </Slider>
          </div>
        </Fade>
      </div>
    </>
  )
}

Card.propTypes = {
  app: PropTypes.object
}
interface CardProps {
  app: AppInfo
  style?: object
}
function Card(props: CardProps) {
  const { t } = useTranslation('homePage')
  const matches = useMediaQuery('(max-width:650px)')

  return (
    <div className={cx(styles.card, { [styles.mobile]: matches })} style={props.style}>
      <div className={styles.top}>
        <h4>{props.app.name}</h4>
        <Image 
          src={props.app.image}
          width={68}
          height={68}
          alt={props.app.name}
        />
      </div>
      <p>
        {props.app.description}
      </p>
      <div className={styles.revenue}>
        {props.app.revenue}
        <br></br>
        {t('blocks.products.bots.revenue_for_month')}
      </div>
      <div className={styles.bottom}>
        <Link href={props.app.stores.appstore}>
          <a>
            <Button 
              outlined 
              buttonProps={{ 
                fullWidth: true,
                // onClick: () => window.open(props.app.stores.appstore)
              }}
            >
              <AppStoreIcon />&nbsp;App Store
            </Button>
          </a>
        </Link>
        <Link href={props.app.stores.googleplay}>
          <a>
            <Button 
              outlined 
              buttonProps={{ 
                fullWidth: true,
                // onClick: () => window.open(props.app.stores.googleplay)
              }}
            >
              <GooglePlayIcon />&nbsp;Google Play
            </Button>
          </a>
        </Link>
      </div>
    </div>
  )
}