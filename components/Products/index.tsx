import styles from './styles.module.scss'
import Subheading from '../Subheading'
import { useTranslation } from 'next-i18next'

import Bots from './Bots'
import Sites from './Sites'
import Apps from './Apps'

export default function Products() {
  const { t } = useTranslation('homePage')
  
  return (
    <div className={styles.products}>
      <Subheading>{t('blocks.products.heading')}</Subheading>
      <Bots />
      <Sites />
      <Apps />
    </div>
  )
}