import PropTypes from 'prop-types'
import React from 'react'
import styles from './styles.module.scss'

Card.propTypes = {
  number: PropTypes.string,
  icon: PropTypes.node,
  text: PropTypes.string
}
interface CardProps {
  number: string
  icon: React.ReactNode
  text: string
}
export default function Card(props: CardProps) {
  return (
    <div className={styles.card}>
      <div className={styles.top}>
        <span className={styles.number}>
          {props.number}
        </span>
        {props.icon}
      </div>
      <div className={styles.bottom}>
        <span className={styles.text}>
          {props.text}
        </span>
      </div>
    </div>
  )
}