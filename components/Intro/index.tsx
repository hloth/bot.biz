import Fade from 'react-reveal/Fade'
import Rotate from 'react-reveal/Rotate'
import { useTranslation } from 'next-i18next'
import rsr from 'react-string-replace'
import styles from './styles.module.scss'
// import PhoneImage from './iPhone.svg'
import PhoneImage from './phone.png'
import Image from 'next/image'
import Card from './Card'

import { ReactComponent as CommunityIcon } from './icons/community.svg'
import { ReactComponent as DialogIcon } from './icons/dialog.svg'
import { ReactComponent as MoneyIcon } from './icons/money.svg'
import { ReactComponent as TimeIcon } from './icons/time.svg'

export default function Intro() {
  
  
  return (
    <div className={styles.grid}>
      <Headline />
      <Phone />
      <Cards />
    </div>
  )
}

function Headline() {
  const { t } = useTranslation('homePage')

  return (
    <Fade bottom cascade>
      <div className={styles.headline}>
        <h1>
          {rsr(
            rsr(
              t('blocks.intro.headline'), /(\%.+?\%)/g, text => <span className={styles.highlight} key={text}>{text.slice(1, -1)}</span>
            ), /(\n)/g, () => <br></br>
          )}
        </h1>
        <p>
          {t('blocks.intro.paragraph')}
        </p>
      </div>
    </Fade>
  )
}

function Phone() {
  return (
    <Fade right>
      <div className={styles.phone}>
        <Image 
          src={PhoneImage} 
          alt='' 
          width={306}
          height={622}
          priority
          quality={100}
        />
      </div>
    </Fade>
  )
}

function Cards() {
  return (
    <div className={styles.cards}>
        <div className={styles.cardsContainer}>
      <Fade bottom left>
        <Card
          number='104'
          icon={<CommunityIcon />}
          text='Количество сообществ'
        />
      </Fade>
      <Fade bottom left>
      <Card
        number='200 тыс.'
        icon={<DialogIcon />}
        text='Сообщений в ботах за месяц'
      />
      </Fade>
      <Fade bottom left>
      <Card
        number='59 400 ₽'
        icon={<MoneyIcon />}
        text='Средний заработок в месяц'
      />
      </Fade>
      <Fade bottom left>
      <Card
        number='30 сек.'
        icon={<TimeIcon />}
        text='Время подключения бота'
      />
      </Fade>
    </div>
    </div>
  )
}