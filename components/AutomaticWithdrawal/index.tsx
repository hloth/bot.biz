import Subheading from '%/components/Subheading'
import styles from './styles.module.scss'
import { useTranslation } from 'next-i18next'
import Button from '%/components/Button'
import Fade from 'react-reveal'

import { ReactComponent as Bank } from './bank.svg'
import { ReactComponent as Electronic } from './electronic.svg'
import { ReactComponent as Mobile } from './mobile.svg'

export default function AutomaticWithdrawal() {
  const { t } = useTranslation('homePage')
  
  return (
    <div className={styles.automaticWithdrawal}>
      <Subheading>{t('blocks.automaticWithdrawal.heading')}</Subheading>
      <Fade bottom>
        <p>
          {t('blocks.automaticWithdrawal.description')}
        </p>
      </Fade>
      <Fade bottom cascade>
      <div className={styles.methods}>
          <div className={styles.method}>
            <span>{t('blocks.automaticWithdrawal.bank')}</span>
            <Bank />
          </div>
          <div className={styles.method}>
            <span>{t('blocks.automaticWithdrawal.electronic')}</span>
            <Electronic />
          </div>
          <div className={styles.method}>
            <span>{t('blocks.automaticWithdrawal.mobile')}</span>
            <Mobile />
          </div>
      </div>
      </Fade>
      <Fade bottom>
        <div className={styles.start}>
          <Button>{t('blocks.start')}</Button>
        </div>
      </Fade>
    </div>
  )
}