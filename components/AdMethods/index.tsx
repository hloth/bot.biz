import PropTypes from 'prop-types'
import styles from './styles.module.scss'
import Subheading from '%/components/Subheading'
import { useTranslation } from 'next-i18next'
import Image from 'next/image'
import Fade from 'react-reveal/Fade'

import Card1 from './CardsAssets/Card1.png'
import Card2 from './CardsAssets/Card2.png'
import Card3 from './CardsAssets/Card3.png'
import { ReactComponent as Megaphone } from './megaphone.svg'
import { ReactComponent as Newspaper } from './newspaper.svg'
import { ReactComponent as Mail } from './mail.svg'
import SectionLine from '%/components/HashScroller/SectionLine'

export default function AdMethods() {
  const { t } = useTranslation('homePage')
  
  return (
    <div className={styles.admethods}>
      <Subheading>{t('blocks.admethods.heading')}</Subheading>
      <SectionLine id='ad-methods' />
      <div className={styles.container}>
        <Decorations />
        <Content />
      </div>
    </div>
  )
}

function Decorations() {
  return (
    <Fade left cascade>
      <div className={styles.decorations}>
        <div className={styles.card1}>
          <Image src={Card1} alt='' priority />
        </div>
        <div className={styles.card2}>
          <Image src={Card2} alt='' priority />
        </div>
        <div className={styles.card3}>
          <Image src={Card3} alt='' priority />
        </div>
      </div>
    </Fade>
  )
}

function Content() {
  return (
    <div className={styles.content}>
      <Method
        icon={<Megaphone />}
        tkey='straightforward'
      />
      <Method 
        icon={<Newspaper />}
        tkey='native'
      />
      <Method 
        icon={<Mail />}
        tkey='spam'
      />
    </div>
  )
}

interface MethodProps {
  icon: React.ReactNode
  tkey: string
}
Method.propTypes = {
  icon: PropTypes.node,
  tkey: PropTypes.string
}
function Method(props: MethodProps) {
  const { t } = useTranslation('homePage')

  return (
    <Fade up cascade>
      <div>
        <span>{props.icon}</span>
        <h6>{t('blocks.admethods.methods.' + props.tkey + '.title')}</h6>
        <p>{t('blocks.admethods.methods.' + props.tkey + '.description')}</p>
      </div>
    </Fade>
  )
}