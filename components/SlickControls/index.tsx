import React from 'react'
import PropTypes from 'prop-types'
import styles from './styles.module.scss'
import Button from '%/components/Button'
import { MdChevronLeft, MdChevronRight } from 'react-icons/md'

SlickControls.propTypes = {
  currentPage: PropTypes.number,
  maxPages: PropTypes.number,
  slideNext: PropTypes.func,
  slideBack: PropTypes.func,
}
interface SlickControlsProps {
  currentPage: number
  maxPages: number
  slideNext?: Function
  slideBack?: Function
}
export default function SlickControls(props: SlickControlsProps) {
  // const [currentPage, setCurrentPage] = React.useState(0)
  // const [maxPages, setMaxPages] = React.useState(0)

  return (
    <div className={styles.container}>
      <span className={styles.pagesCounter}>
        <span>{props.currentPage + 1}</span>/{props.maxPages}
      </span>
      <Button icon buttonProps={{ onClick: () => props.slideBack?.() }}><MdChevronLeft /></Button>
      <Button outlined icon buttonProps={{ onClick: () => props.slideNext?.() }}><MdChevronRight /></Button>
    </div>
  )
}