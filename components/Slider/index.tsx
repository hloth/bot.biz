import React from 'react'
import PropTypes from 'prop-types'
import styles from './styles.module.scss'

interface SliderProps {
  afterChange: Function
  children: React.ReactNode
  className: string
}
const Slider = React.forwardRef(({ afterChange, ...props }: SliderProps, ref) => {
  const sliderRef = React.useRef()
  const [currentSlide, setCurrentSlide] = React.useState(0)

  const getIndex = (root: HTMLDivElement): number => {
    let currentIndex = 0, sumX = 0
    const children = Array.from(root.children)
    for (let child of children) {
      sumX += child.offsetWidth
      if (sumX > root.scrollLeft) break
      currentIndex++
    }
    return currentIndex
  }

  React.useImperativeHandle(ref, () => {
    const scroll = (delta) => {
      const currentIndex = getIndex(sliderRef.current)
      const nextScrollLeft: number = Array.from(sliderRef.current.children)
        .slice(0, currentIndex + delta)
        .reduce(
          (prev: number, cur: HTMLDivElement) => prev + cur.offsetWidth,
          0)
      sliderRef.current?.scrollTo?.({
        left: nextScrollLeft,
        behavior: 'smooth'
      })
    }

    return {
      prev() {
        scroll(-1)
      },

      next() {
        scroll(+1)
      }
    }
  }, [sliderRef])

  React.useEffect(() => {
    if (!sliderRef.current) return
    const scrollEvent = event => {
      let currentIndex = getIndex(event.target)
      setCurrentSlide(
        Math.max(
          Math.min(currentIndex, props.children.length) - 1, 
        0)
      )
    }
    sliderRef.current.addEventListener('scroll', scrollEvent)
    return () => sliderRef.current?.removeEventListener('scroll', scrollEvent)
  }, [sliderRef])

  React.useEffect(() => {
    afterChange(currentSlide)
  }, [afterChange, currentSlide])

  return (
    <div className={[styles.slider, props.className].join(' ')} ref={sliderRef}>
      <div className={styles.start} />
      {props.children}
      <div><div className={styles.safariFixEnd} /></div>
    </div>
  )
})
Slider.propTypes = {
  afterChange: PropTypes.func,
  children: PropTypes.node,
  className: PropTypes.string
}
Slider.displayName = 'Slider'
export default Slider