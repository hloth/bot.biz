import React, { ChangeEvent, InputHTMLAttributes, useState } from 'react'
import cn from 'classnames'

import styles from './styles.module.scss'

export interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  inputClassName?: string | undefined
  center?: boolean
}

export default function Input({
  className,
  inputClassName,
  center,
  type = 'text',
  value,
  onChange,
  ...restProps
}: InputProps) {
  const [inputValue, setInputValue] = useState(value)

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { maxLength } = event.target
    let newValue: string | number = event.target.value

    if (maxLength > 0) {
      newValue = newValue.slice(0, maxLength)
    }

    setInputValue(newValue)

    if (onChange) {
      onChange(event)
    }
  }

  return (
    <div className={cn(styles.inputContainer, className)}>
      <input
        {...restProps}
        type={type}
        className={cn(styles.input, inputClassName, { [styles.inputCenter]: center })}
        value={inputValue}
        onChange={handleChange}
      />
    </div>
  )
}
