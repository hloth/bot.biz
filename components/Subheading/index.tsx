import Fade from 'react-reveal/Fade'
import styles from './styles.module.scss'

export default function Subheading(props) {
  return (
    <h2 className={styles.text}>
      <Fade bottom>
        <span className={styles.preTextDecoration} />
        {props.children}
      </Fade>
    </h2>
  )
}