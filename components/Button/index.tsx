import React from 'react'
import PropTypes from 'prop-types'
import MUIButton from '@mui/material/Button'
import styles from './styles.module.scss'
import cx from 'classnames'

Button.propTypes = {
  outlined: PropTypes.bool,
  children: PropTypes.node,
  icon: PropTypes.bool,
  buttonProps: PropTypes.object,
  style: PropTypes.object
}
interface ButtonProps {
  outlined?: boolean
  icon?: boolean
  children: React.ReactNode
  buttonProps?: object
  style?: object
}
export default function Button(props: ButtonProps) {
  const [pressed, setPressed] = React.useState(false)

  return (
    <div
      onPointerDown={() => setPressed(true)}
      onPointerUp={() => setPressed(false)}
      onPointerOut={() => setPressed(false)}
      className={styles.buttonContainer}
      style={props.style}
    >
      <div className={cx(styles.button, { [styles.pressed]: pressed })}>
        <MUIButton
          className={cx(styles.muiButton, {
            [styles.contained]: !props.outlined,
            [styles.outlined]: props.outlined,
            [styles.icon]: props.icon
          })}
          {...props.buttonProps}
        >
          {props.children}
        </MUIButton>
      </div>
    </div>
  )
}