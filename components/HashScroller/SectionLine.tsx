export default function SectionLine(props: { id: string }) {
  return (
    <div id={props.id} style={{ position: 'absolute', marginTop: -80 }} />
  )
}