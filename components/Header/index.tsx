import React from 'react'
import styles from './styles.module.scss'
import { useAppDispatch, useAppSelector } from "%/store/hooks"
import { selectTheme, switchTheme } from "%/store/reducers/theme"
import Logo from '../Logo'
import { ReactComponent as LogoIcon } from '../Logo/logo.svg'
import cx from 'classnames'
import { useScrollPosition } from '@n8tb1t/use-scroll-position'
import Button from '%/components/Button'
import { useTranslation } from 'next-i18next'
import ThemeSwitcher from './ThemeSwitcher'
import Hamburger from 'hamburger-react'

import addHashChangeListener from '../HashScroller'

function Header(props) {
  const { t } = useTranslation('common')
  const [scrollY, setScrollY] = React.useState(1)
  const [mobileMenuShown, setMobileMenuShown] = React.useState(false)

  useScrollPosition(scrollPosition => {
    setScrollY(-scrollPosition.currPos.y)
  }, [setScrollY])

  React.useEffect(() => {
    if(!typeof window) return
    addHashChangeListener()
    setScrollY(window.scrollY)
  }, [])

  React.useEffect(() => {
    if (mobileMenuShown) {
      document.documentElement.classList.add('noOverflow')
    } else {
      document.documentElement.classList.remove('noOverflow')
    }
  }, [mobileMenuShown])

  return (
    <header className={cx(styles.header, { [styles.shrink]: scrollY > 0 })}>
      <div className={styles.container}>
        <div className={styles.left}>
          <div className={styles.mobileMenuButton}>
            <Hamburger
              toggled={mobileMenuShown}
              onToggle={() => setMobileMenuShown(!mobileMenuShown)}
            />
            <MobileMenu show={mobileMenuShown} setMobileMenuShown={setMobileMenuShown} />
          </div>
          <div className={styles.mobileThemeSwitcher}>
            <ThemeSwitcherContainer />
          </div>
          <div className={styles.leftContent}>
            <Logo importedLogo={<LogoIcon />} />
            <Navigation />
          </div>
        </div>
        <div className={styles.right}>
          <ThemeSwitcherContainer />
          <div className={styles.signOptions}>
            <button className={styles.registration}>{t('header.registration')}</button>
            <Button>{t('header.login')}</Button>
          </div>
        </div>
      </div>
    </header>
  )
}

function Navigation(props: { onLinkClicked?: Function }) {
  const { t } = useTranslation('common')

  return (
    <div className={styles.navigation}>
      <a href='#bots' onClick={() => props.onLinkClicked?.()}>{t('header.navigation.bots')}</a>
      <a href='#connection' onClick={() => props.onLinkClicked?.()}>{t('header.navigation.connection')}</a>
      <a href='#methods' onClick={() => props.onLinkClicked?.()}>{t('header.navigation.methods')}</a>
      <a href='#functions' onClick={() => props.onLinkClicked?.()}>{t('header.navigation.functions')}</a>
    </div>
  )
}

interface MobileMenuProps {
  show: boolean
  setMobileMenuShown: Function
}
function MobileMenu(props: MobileMenuProps) {
  return (
    <div className={cx(styles.mobileMenu, { [styles.show]: props.show })}>
      <Navigation onLinkClicked={() => props.setMobileMenuShown(false)} />
    </div>
  )
}

function ThemeSwitcherContainer() {
  const dispatch = useAppDispatch()
  const theme = useAppSelector(selectTheme)

  return (
    <div className={styles.themeSwitcher}>
      <ThemeSwitcher onSwitch={() => dispatch(switchTheme())} />
    </div>
  )
}

export default Header