import type { NextPage } from 'next'
import Head from '%/components/Head'
import dynamic from 'next/dynamic'
import Loading from '%/components/Loading'
import styles from '%/styles/Home.module.scss'
import config from 'react-reveal/globals'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useTranslation } from 'next-i18next'
import '%/components/ThemeLoader'

import Header from '%/components/Header'
import Intro from '%/components/Intro'
import Products from '%/components/Products'
import HowItWorks from '%/components/HowItWorks'
import AdMethods from '%/components/AdMethods'
import UsefulFunctions from '%/components/UsefulFunctions'
import AutomaticWithdrawal from '%/components/AutomaticWithdrawal'
import Footer from '%/components/Footer'

config({ ssrFadeout: true })
const EventsLoader = dynamic(() => import('../components/DeferredNoSSR'))

const Home: NextPage = () => {
  config({ ssrFadeout: true })
  const { t } = useTranslation('homePage')

  return (
    <>
      <Head 
        title={t('title')}
        description={t('description')}
      />
      <EventsLoader />
      <Loading />
      <Header />
      <div className={styles.container}>
        <Intro />
        <Products />
        <HowItWorks />
        <AdMethods />
        <UsefulFunctions />
        <AutomaticWithdrawal />
      </div>
      <Footer />
    </>
  )
}

export default Home

export async function getStaticProps(context: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(context.locale, ['common', 'homePage'])),
    }
  }
}
