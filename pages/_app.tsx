import '../styles/globals.scss'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

import type { AppProps } from 'next/app'
import { appWithTranslation } from 'next-i18next'
import { Provider } from 'react-redux'
import { store } from '%/store'
import { CacheProvider } from '@emotion/react'
import { ThemeProvider, CssBaseline } from '@mui/material'
import createEmotionCache from '%/components/MUITheme/emotionCache'
import theme from '%/components/MUITheme'

const clientSideEmotionCache = createEmotionCache()

function BotBizApp({ Component, emotionCache = clientSideEmotionCache, pageProps }: AppProps) {

  return (
  <Provider store={store}>
    <CacheProvider value={emotionCache}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Component {...pageProps} />
      </ThemeProvider>
    </CacheProvider>
  </Provider>
  )
}

export default appWithTranslation(BotBizApp)