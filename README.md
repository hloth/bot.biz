# Bot.biz Landing Page

<img src="https://ik.imagekit.io/hloth/image.png?updatedAt=1691362487698" />

Landing page made for x5 (my previous employer) in 2022.

Supports dynamic color theme with CSS transitions, scroll effects and native sliders with "magnetic" css descriptors.

Visit deployed version at [botbiz-landing.netlify.app](https://botbiz-landing.netlify.app)